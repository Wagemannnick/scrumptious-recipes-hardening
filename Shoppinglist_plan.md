# Shopping List Four Features
    1. A "+ shopping list" button next to any ingredient not already in the shopping list that, when clicked, adds it to the shopping list
    2.  main nav link that links to a list of the current items in the shopping list with an indicator of how many items are in the shopping list
    3.  The actual shopping list of food items that have been added
    4.  A button on the list page to clear all items from the shopping list




----
# Create new Model Shopping List
    -[x] User - AUTH_USER_MODEL - foreign key - Model on delete=cascade
    -[x] FoodItem - foreignkey FoodItem - on delete protect
    -[x] make migrations / migrate

# Create Paths
-[x]Shopping_items/create -Adds shopping item on the users list
-[x]Shopping_items -List of shopping items on the list
-[x]shopping_items/delete -Clears entire shopping list

# Create Views
-[x] List View 
    -[x]show list of shopping items
    -[x]not show all shopping lists singular list
    -[x]deletes all items button
-[ ] Create View
    -[x] No Html Template
    -[x] Shopping Item instance in database on current user and the value of submitted"ingredients_id"
    -[x] redirects to recipe page
-[ ] Delete View
    -[x] No Html template
    -[x] Delete Shopping Item instances for user
    -[x] Show Empty List 