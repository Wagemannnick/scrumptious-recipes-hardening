# My Plan

- [x] Make App
        make sure to run start app

- [x] Make Meal Plan Model
    - name 
    - date
    - owner
    - recipes

- [x] Make Meal Plan Admin

- [x] Meal Plan Paths
        -meal_plans/
        -meal_plans/create/
        -meal_plans/<int:pk>/
        -meal_plans/<int:pk>/edit/
        -meal_plans/<int:pk>/delete/

- [x] Each path points to view
  - [x] Create View
    - [x] Delete View
    - [x] Edit View
    - [x] List View
    - [x] Detail View 

- [x] Meal Plan Templates
  - [x] All templates should extend from base.html
  - [x] Login Link should show in nav when user is not authenticated
  
- [ ] Create Templates and insert view functions
  - [x] List View
  - [x] Detail View
  - [x] Create View
    - [x] show form that allows users to enter name/date/pick recipe 
    - [x] when user saves the object will be saved to owner property(RecipeCreateView Reference)
    - [x] page should redirect to the newly created page detail page
    - [x] find extra piece of code that you must include to make sure recipes are saved with the meal plan (save_m2m search)
  - [x] Edit View
  - [x] Delete View
  




