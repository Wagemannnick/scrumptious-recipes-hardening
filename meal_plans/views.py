from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from meal_plans.models import MealPlans

from django.contrib.auth.mixins import LoginRequiredMixin


class MealPlansListView(ListView):
    model = MealPlans
    template_name = "meal_plans/list.html"
    paginate_by = 5

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlans
    template_name = "meal_plans/new.html"
    fields = ["name", "date", "recipes"]

    def form_valid(self, form):
        meal_plans = form.save(commit=False)
        meal_plans.owner = self.request.user
        meal_plans.save()
        form.save_m2m()
        return redirect("meal_plans_detail", pk=meal_plans.id)


class MealPlansDetailView(DetailView):
    model = MealPlans
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlans.objects.filter(owner=self.request.user)


class MealPlansUpdateView(UpdateView):
    model = MealPlans
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plans_list")


class MealPlansDeleteView(DeleteView):
    model = MealPlans
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("meal_plans_list")
