# Generated by Django 4.0.3 on 2022-06-08 22:34

import datetime
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0008_recipe_author'),
        ('meal_plans', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MealPlans',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120)),
                ('date', models.DateTimeField(default=datetime.datetime.now)),
                ('owner', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('recipes', models.ManyToManyField(related_name='recipes', to='recipes.recipe')),
            ],
        ),
    ]
