from django.contrib import admin

from meal_plans.models import MealPlans


class MealPlanAdmin(admin.ModelAdmin):
    pass


admin.site.register(MealPlans, MealPlanAdmin)
