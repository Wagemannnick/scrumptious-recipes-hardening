from django.db import models
from datetime import datetime

from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class MealPlans(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField(default=datetime.now)
    owner = models.ForeignKey(USER_MODEL, on_delete=models.CASCADE, null=True)
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")

    def __str__(self):
        return self.name
